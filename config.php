<?php

/*
 * Repositories configurations
 * Deployer Script by Rodrigo Borges <rbm0407@gmail.com>
 * @TODO Change the config before start using deployer
 */

$repositories = array(
    'repositoryname' =>
        array(
        	'remotePath' => 'bitbucketusername/repositoryname',
            'location' => '/var/www/serverdirorrepositoryname'
        ),
    'anotherrepositoryname' =>
    	array(
    		'remotePath' => 'bitbucketusername/anotherrepositoryname',
    		'location' => '/var/www/anotherserverdiroranotherrepositoryname'
		)
);