# deployer #

Simple bitbucket webhook receiver to pull changes automatic

### Prerequisites ###

* PHP >= v5
* Target repository configured with SSH (doesn't work with https protocol)
* SSH keys configured and ssh-agent running
* Deployment key on target repository

[Learn more how to configure SSH with Bitbucket](https://confluence.atlassian.com/bitbucket/use-the-ssh-protocol-with-bitbucket-cloud-221449711.html)

### Setup ###

* clone the project on the server
* add the repository name and location on config.php
* look what port the server will run
* go to project Settings and add your server on webhooks
* run the server.sh (the script will run on background)
* push the repository that you configured

### Authors ###

* @rbm0407