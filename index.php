<?php // Deployer Script by Rodrigo Borges <rbm0407@gmail.com>
require_once 'config.php';

if(!isset($repositories))
{
    echo 'Error, list of repositories not found';
    exit;
}

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: X-Requested-With');

$inputRequest = file_get_contents('php://input');

if(!$inputRequest)
{
    echo 'Nothing to do';
    exit;
}

$input = json_decode($inputRequest);


if(!isset($input->repository))
{
    echo 'Nothing to do';
    exit;
}

if(isset($repositories[$input->repository->name]))
{
    $repConfig = $repositories[$input->repository->name];

    if( !isset($repConfig['location']) )
    {
        echo 'Missing configuration values';
        exit;
    }

    $repConfigLocation = $repConfig['location'];

    $system = 'cd '. $repConfigLocation .' && git pull 2>&1';
    $a = system($system);
    echo $a;

}
else
{
    echo 'Repository not combine with local configuration';
    exit;
}